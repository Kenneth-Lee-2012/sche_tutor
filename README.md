这个工程包含《[理解调度器](https://cpp-aux-tutorial.readthedocs.io/zh-cn/latest/18.html)》一文的例子程序。

主要是通过几个用户态的程序，说明调度器的工作原理。

这个程序当前的设计意图主要是在64位PC的Linux环境中运行。
