/* Copyright by Kenneth Lee, 2024. All Rights Reserved */

#include <stdio.h>

int job1(void) {
	int i, sum;
	for(i=0,sum=0; i<100; i++) {
		sum+=i;
	}
	return sum;
}

int job2(void) {
	int i, sum;
	for(i=0,sum=0; i<100; i++) {
		sum+=i*2;
	}
	return sum;
}

int job3(void) {
	int i, sum;
	for(i=0,sum=0; i<100; i++) {
		sum+=i*3;
	}
	return sum;
}

struct {
	int priority;
	int (*job)(void);
	int done;
} batch_jobs[] = {
	{ .priority = 10, .job = job1, .done = 0 },
	{ .priority = 12, .job = job2, .done = 0 },
	{ .priority = 9,  .job = job3, .done = 0 }
};

int batch_sched(void) {
	int i, ret;
	for (i = 0; i < sizeof(batch_jobs)/sizeof(batch_jobs[0]); i++) {
		if (!batch_jobs[i].done) {
			ret = batch_jobs[i].job();
			printf("job %d finished with %d\n", i, ret);
			batch_jobs[i].done = 1;
			return 1;
		}
	}
	return 0;
}

int main(void) {
	while(batch_sched());
	return 0;
}
