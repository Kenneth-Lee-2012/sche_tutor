CFLAGS=-Wall -g
ALL=batch_sched cothread_sched

all: $(ALL)

switch_asm.o: switch_asm.S
cothread_sched.o: cothread_sched.c

batch_sched: batch_sched.c

cothread_sched: cothread_sched.o switch_asm.o
	gcc $^ -o $@

.PHONY: clean all

clean:
	rm -f $(ALL) *.o
